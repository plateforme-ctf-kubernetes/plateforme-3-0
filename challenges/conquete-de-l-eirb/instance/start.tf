provider "helm" {
  kubernetes {
    config_path = "/app/rke2/rke2.yaml"
  }
}

provider "kubernetes" {
  config_path = "/app/rke2/rke2.yaml"
}

variable "build_version" {
  type = string
}

variable "container_registry" {
  type = string
}

variable "instance_id" {
  type = number
}

resource "helm_release" "cqteirb" {
  name         = "helm-${var.instance_id}-cqteirb"
  chart        = "${path.module}/helm-chart-scenario-cqteirb"
  force_update = true

  set {
    name  = "namespace"
    value = "kubernetes-scenario-cqteirb-${var.instance_id}"
    type = "string"
  }

  set {
    name  = "build_version"
    value = "${var.build_version}"
    type  = "string"
  }

  set {
    name  = "container_registry"
    value = "${var.container_registry}"
    type  = "string"
  }

  set {
    name  = "flag_cqteirb_oeno_user"
    value = "FLAG{${random_string.flag_cqteirb_oeno_user.result}}"
  }

  set {
    name  = "flag_cqteirb_oeno_root"
    value = "FLAG{${random_string.flag_cqteirb_oeno_root.result}}"
  }

  set {
    name  = "flag_cqteirb_pixeirb_user"
    value = "FLAG{${random_string.flag_cqteirb_pixeirb_user.result}}"
  }

  set {
    name  = "flag_cqteirb_pixeirb_root"
    value = "FLAG{${random_string.flag_cqteirb_pixeirb_root.result}}"
  }

  set {
    name  = "flag_cqteirb_eirbware_root"
    value = "FLAG{${random_string.flag_cqteirb_eirbware.result}}"
  }

  set {
    name  = "flag_cqteirb_asterisk"
    value = "FLAG{${random_string.flag_cqteirb_asterisk.result}}"
  }
}

resource "kubernetes_service" "cqteirb-service" {
  metadata {
    name = "cqteirb-service"
    namespace = "kubernetes-scenario-cqteirb-${var.instance_id}"
  }
  spec {
    type = "NodePort"
    port {
      port = "22"
      target_port = "22"
    }
    selector = {
      app = "kali"
    }
  }
  depends_on = [helm_release.cqteirb]
}

# Génération du flag user
resource "random_string" "flag_cqteirb_oeno_user" {
  keepers     = { instance_id = "${var.instance_id}_cqteirb_oeno_user" }
  length      = 16
  special     = false
  min_upper   = 4
  min_lower   = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag_cqteirb_oeno_user.result} > ${path.module}/flags/flag_cqteirb_oeno_user.txt"
  }
}

# Génération du flag root
resource "random_string" "flag_cqteirb_oeno_root" {
  keepers     = { instance_id = "${var.instance_id}_cqteirb_oeno_root" }
  length      = 16
  special     = false
  min_upper   = 4
  min_lower   = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag_cqteirb_oeno_root.result} > ${path.module}/flags/flag_cqteirb_oeno_root.txt"
  }
}

# Génération du flag user
resource "random_string" "flag_cqteirb_pixeirb_user" {
  keepers     = { instance_id = "${var.instance_id}_cqteirb_pixeirb_user" }
  length      = 16
  special     = false
  min_upper   = 4
  min_lower   = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag_cqteirb_pixeirb_user.result} > ${path.module}/flags/flag_cqteirb_pixeirb_user.txt"
  }
}

# Génération du flag root
resource "random_string" "flag_cqteirb_pixeirb_root" {
  keepers     = { instance_id = "${var.instance_id}_cqteirb_pixeirb_root" }
  length      = 16
  special     = false
  min_upper   = 4
  min_lower   = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag_cqteirb_pixeirb_root.result} > ${path.module}/flags/flag_cqteirb_pixeirb_root.txt"
  }
}

# Génération du flag
resource "random_string" "flag_cqteirb_eirbware" {
  keepers     = { instance_id = "${var.instance_id}_cqteirb-eirbware" }
  length      = 16
  special     = false
  min_upper   = 4
  min_lower   = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag_cqteirb_eirbware.result} > ${path.module}/flags/flag_cqteirb-eirbware.txt"
  }
}

# Génération du flag
resource "random_string" "flag_cqteirb_asterisk" {
  keepers     = { instance_id = "${var.instance_id}_cqteirb-asterisk" }
  length      = 16
  special     = false
  min_upper   = 4
  min_lower   = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag_cqteirb_asterisk.result} > ${path.module}/flags/flag_cqteirb-asterisk.txt"
  }
}

output "port" {
  value = kubernetes_service.cqteirb-service.spec[0].port[0].node_port
}