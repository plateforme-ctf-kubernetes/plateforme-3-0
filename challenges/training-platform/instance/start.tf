provider "helm" {
  kubernetes {
    config_path = "/app/rke2/rke2.yaml"
  }
}

provider "kubernetes" {
  config_path = "/app/rke2/rke2.yaml"
}

variable "build_version" {
  type = string
}

variable "container_registry" {
  type = string
}

variable "instance_id" {
  type = number
}

resource "helm_release" "helm-chart-training-plateform" {
  name       = "helm-${var.instance_id}-training-plateform"
  chart      = "${path.module}/helm-chart-training-plateform"

  set {
    name  = "build_version"
    value = "${var.build_version}"
    type  = "string"
  }

  set {
    name  = "namespace"
    value = "kubernetes-training-plateform-${var.instance_id}"
    type  = "string"
  }

  set {
    name  = "container_registry"
    value = "${var.container_registry}"
    type  = "string"
  }

}

resource "kubernetes_service" "training-plateform-service" {
  metadata {
    name = "service-training-plateform"
    namespace = "kubernetes-training-plateform-${var.instance_id}"
  }
  spec {
    type = "NodePort"
    port {
      port = "80"
      target_port = "80"
    }
    selector = {
      app = "training-plateform"
    }
  }
  depends_on = [helm_release.helm-chart-training-plateform]
}

output "port" {
  value = kubernetes_service.training-plateform-service.spec[0].port[0].node_port
}
