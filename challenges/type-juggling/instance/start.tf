provider "helm" {
  kubernetes {
    config_path = "/app/rke2/rke2.yaml"
  }
}

provider "kubernetes" {
  config_path = "/app/rke2/rke2.yaml"
}

variable "build_version" {
  type = string
}

variable "container_registry" {
  type = string
}

variable "instance_id" {
  type = number
}

resource "helm_release" "helm-chart-challenge-type-juggling" {
  name         = "helm-${var.instance_id}-type-juggling"
  chart        = "${path.module}/helm-chart-challenge-type-juggling"

  set {
    name  = "instance_id"
    value = "${var.instance_id}"
  }

  set {
    name  = "build_version"
    value = "${var.build_version}"
    type  = "string"
  }

  set {
    name  = "namespace"
    value = "kubernetes-challenge-type-juggling-${var.instance_id}"
    type  = "string"
  }

  set {
    name  = "container_registry"
    value = "${var.container_registry}"
    type  = "string"
  }

  set {
    name  = "flag"
    value = "<?php $FLAG = '${random_string.flag.result}'; "
  }
}

resource "kubernetes_service" "type-juggling-service" {
  metadata {
    name = "type-juggling-service"
    namespace = "kubernetes-challenge-type-juggling-${var.instance_id}"
  }
  spec {
    type = "NodePort"
    port {
      port = "80"
      target_port = "80"
    }
    selector = {
      app = "type-juggling"
    }
  }
  depends_on = [helm_release.helm-chart-challenge-type-juggling]
}

#
# Génération du flag
#
resource "random_string" "flag" {
  keepers = {instance_id = var.instance_id }
  length = 16
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag.result} > ${path.module}/flags/flag.txt"
  }
}

output "port" {
  value = kubernetes_service.type-juggling-service.spec[0].port[0].node_port
}