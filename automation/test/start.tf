provider "helm" {
  kubernetes {
    config_path = "~/rke2.yaml"
  }
}

variable "build_version" {
  type = string
}

variable "container_registry" {
  type = string
}

resource "helm_release" "helm-chart-plateforme-ctf" {
  name       = "helm-chart-plateforme-ctf"
  chart      = "${path.module}/../helm-chart-plateforme-ctf"

  set {
    name  = "container_registry"
    value = "${var.container_registry}"
    type  = "string"
  }

  set {
    name  = "build_version"
    value = "${var.build_version}"
    type  = "string"
  }

  set {
    name  = "host_address"
    value = "ctf.rsr.kubernetes.enseirb-matmeca.fr"
    type  = "string"
  }
}
