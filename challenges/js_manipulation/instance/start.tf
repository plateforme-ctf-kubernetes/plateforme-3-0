provider "helm" {
  kubernetes {
    config_path = "/app/rke2/rke2.yaml"
  }
}

provider "kubernetes" {
  config_path = "/app/rke2/rke2.yaml"
}

variable "build_version" {
  type = string
}

variable "container_registry" {
  type = string
}

variable "instance_id" {
  type = number
}

resource "helm_release" "helm-chart-challenge-js-manipulation" {
  name         = "helm-${var.instance_id}-js-manipulation"
  chart        = "${path.module}/helm-chart-challenge-js-manipulation"

  set {
    name  = "build_version"
    value = "${var.build_version}"
    type  = "string"
  }

  set {
    name  = "namespace"
    value = "kubernetes-challenge-js-manipulation-${var.instance_id}"
    type  = "string"
  }

  set {
    name  = "container_registry"
    value = "${var.container_registry}"
    type  = "string"
  }
}

resource "kubernetes_service" "js-manipulation-service" {
  metadata {
    name = "js-manipulation-service"
    namespace = "kubernetes-challenge-js-manipulation-${var.instance_id}"
  }
  spec {
    type = "NodePort"
    port {
      port = "80"
      target_port = "80"
    }
    selector = {
      app = "js-manipulation"
    }
  }
  depends_on = [helm_release.helm-chart-challenge-js-manipulation]
}

output "port" {
  value = kubernetes_service.js-manipulation-service.spec[0].port[0].node_port
}

# endregion
