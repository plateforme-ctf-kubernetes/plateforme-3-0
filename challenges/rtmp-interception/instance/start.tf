provider "helm" {
  kubernetes {
    config_path = "/app/rke2/rke2.yaml"
  }
}

provider "kubernetes" {
  config_path = "/app/rke2/rke2.yaml"
}

variable "build_version" {
  type = string
}

variable "container_registry" {
  type = string
}

variable "instance_id" {
  type = number
}


resource "helm_release" "rtmp-interception" {
  name         = "helm-${var.instance_id}-rtmp-interception"
  chart        = "${path.module}/helm-chart-challenge-rtmp-interception"
  force_update = true


  set {
    name  = "build_version"
    value = "${var.build_version}"
    type  = "string"
  }

  set {
    name  = "container_registry"
    value = "${var.container_registry}"
    type  = "string"
  }

  set {
    name  = "namespace"
    value = "kubernetes-challenge-rtmp-interception-${var.instance_id}"
    type = "string"
  }
}

resource "kubernetes_service" "rtmp-interception-service" {
  metadata {
    name = "rtmp-interception-service"
    namespace = "kubernetes-challenge-rtmp-interception-${var.instance_id}"
  }
  spec {
    type = "NodePort"
    port {
      port = "22"
      target_port = "22"
    }
    selector = {
      app = "kali"
    }
  }
  depends_on = [helm_release.rtmp-interception]
}

output "port22_kali" {
  value = kubernetes_service.rtmp-interception-service.spec[0].port[0].node_port
}