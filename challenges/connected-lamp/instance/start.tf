provider "helm" {
  kubernetes {
    config_path = "/app/rke2/rke2.yaml"
  }
}

provider "kubernetes" {
  config_path = "/app/rke2/rke2.yaml"
}

variable "build_version" {
  type = string
}

variable "container_registry" {
  type = string
}

variable "instance_id" {
  type = number
}


resource "helm_release" "connected-lamp" {
  name         = "helm-${var.instance_id}-connected-lamp"
  chart        = "${path.module}/helm-chart-challenge-connected-lamp"
  force_update = true


  set {
    name  = "build_version"
    value = "${var.build_version}"
    type  = "string"
  }

  set {
    name  = "container_registry"
    value = "${var.container_registry}"
    type  = "string"
  }

  set {
    name  = "namespace"
    value = "kubernetes-challenge-connected-lamp-${var.instance_id}"
    type = "string"
  }

  set {
    name  = "flag"
    value = "${random_string.flag.result}"
  }
}

resource "kubernetes_service" "connected-lamp-service" {
  metadata {
    name = "connected-lamp-service"
    namespace = "kubernetes-challenge-connected-lamp-${var.instance_id}"
  }
  spec {
    type = "NodePort"
    port {
      port = "22"
      target_port = "22"
    }
    selector = {
      app = "kali"
    }
  }
  depends_on = [helm_release.connected-lamp]
}

#
# Génération du flag
#
resource "random_string" "flag" {
  keepers = {instance_id = var.instance_id }
  length = 16
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag.result} > ${path.module}/flags/flag.txt"
  }
}

output "port22_kali" {
  value = kubernetes_service.connected-lamp-service.spec[0].port[0].node_port
}